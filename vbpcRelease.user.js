// ==UserScript==
// @name		VB-Paradise Kommentarplugin
// @version		3.0.0
// @namespace	tforge.vbpc
// @description	Die Kommentarfunktion für VB-Paradise
// @run-at		document-end
//
// @include		https://vbpc.tforge.de/*
// @include		http://vbpc.tforge.de/*
// @include		https://www.vb-paradise.de/*
// @include		http://www.vb-paradise.de/*
//
// @downloadURL	https://vbpc.tforge.de/vbpc.user.js
// @updateURL	https://vbpc.tforge.de/vbpc.meta.js
// ==/UserScript==

var baseUrl = "xmpp.tforge.de";

if (document.location.hostname === "www.vb-paradise.de") {
	var s = document.createElement("script");
	s.type = "text/javascript";
	s.src = document.location.protocol + "//" + baseUrl + "/client/js/script.js";
	s.id = "vbpc-client";
	document.head.appendChild(s);
}
