<?php
namespace wcf\system;
use wcf\system\exception\IPrintableException;
use wcf\system\exception\LoggedException;
use wcf\system\exception\SystemException;

// define current unix timestamp
define('TIME_NOW', time());

// set exception handler
//set_exception_handler(array('wcf\system\WCF', 'handleException'));

// set php error handler
set_error_handler(array('wcf\system\WCF', 'handleError'), E_ALL);

// set autoloader
spl_autoload_register(array('wcf\system\WCF', 'autoload'));

/**
 * WCF is the central class for the community framework.
 * It holds the database connection.
 * 
 * @author	Marcel Werk
 * @copyright	2001-2014 WoltLab GmbH
 * @license	GNU Lesser General Public License <http://opensource.org/licenses/lgpl-license.php>
 * @package	com.woltlab.wcf
 * @subpackage	system
 * @category	Community Framework
 */
class WCF {
	/**
	 * list of autoload directories
	 * @var	array
	 */
	protected static $autoloadDirectories = array();

	/**
	 * database object
	 * @var	\wcf\system\database\Database
	 */
	protected static $dbObj = null;

	/**
	 * Calls all init functions of the WCF class.
	 */
	public function __construct() {
		// add autoload directory
		self::$autoloadDirectories['wcf'] = WCF_DIR . 'lib/';
		self::$autoloadDirectories['vbpc'] = VBPC_DIR . 'lib/';

		$this->initDB();
	}

	/**
	 * Returns the database object.
	 *
	 * @return \wcf\system\database\Database
	 */
	public static function getDB() {
		return self::$dbObj;
	}

	/**
	 * Loads the database configuration and connects to the database.
	 */
	protected function initDB() {
		// get configuration
		$dbHost = $dbUser = $dbPassword = $dbName = '';
		$dbPort = 0;
		$dbClass = 'wcf\system\database\MySQLDatabase';
		require(WCF_DIR.'config.inc.php');
		
		// create database connection
		self::$dbObj = new $dbClass($dbHost, $dbUser, $dbPassword, $dbName, $dbPort);
	}

	/**
	 * True if debugging is enabled, otherwise false.
	 *
	 * @return bool
	 */
	public static function debugModeIsEnabled() {
		return defined('WCF_DEBUG_MODE') && WCF_DEBUG_MODE;
	}

	/**
	 * Includes the required util or exception classes automatically.
	 * 
	 * @param	string		$className
	 * @see		spl_autoload_register()
	 */
	public static final function autoload($className) {
		$namespaces = explode('\\', $className);
		if (count($namespaces) > 1) {
			$applicationPrefix = array_shift($namespaces);
			if ($applicationPrefix === '') {
				$applicationPrefix = array_shift($namespaces);
			}
			if (isset(self::$autoloadDirectories[$applicationPrefix])) {
				$classPath = self::$autoloadDirectories[$applicationPrefix] . implode('/', $namespaces) . '.class.php';
				if (file_exists($classPath)) {
					require_once($classPath);
				}
			}
		}
	}

	/**
	 * Prints an exception.
	 * 
	 * @param	\Exception	$e
	 */
	public static final function handleException($e) {
		
		if(self::debugModeIsEnabled()) {
			if($e instanceof LoggedException) {
				echo htmlentities($e->getMessage());
			} else {
				echo 'Unhandled '.get_class($e)."\n".
					 'Message: '.$e->getMessage()."\n".
					 'File: '.$e->getFile().' ('.$e->getLine().")\n".
					 "Stacktrace: \n  ".implode("\n  ", explode("\n", $e->getTraceAsString()))."\n";
			}
		} else {
			echo htmlentities($e->getMessage());
			/*
			if($e instanceof IPrintableException) {
				echo htmlentities($e->getMessage());
			} else if($e instanceof LoggedException) {
				echo '{"success":false, "code":"ERR_EXCEPTION", "id":"'.$e->getExceptionID().'"}';
			} else {
				echo '{"success":false, "code":"ERR_EXCEPTION"}';
			}
			*/
		}

		exit;
	}
	
	/**
	 * Catches php errors and throws a system exception instead.
	 * 
	 * @param	integer		$errorNo
	 * @param	string		$message
	 * @param	string		$filename
	 * @param	integer		$lineNo
	 */
	public static final function handleError($errorNo, $message, $filename, $lineNo) {
		if (error_reporting() != 0) {
			$type = 'error';
			switch ($errorNo) {
				case 2: $type = 'warning';
					break;
				case 8: $type = 'notice';
					break;
			}
			
			throw new SystemException('PHP '.$type.' in file '.$filename.' ('.$lineNo.'): '.$message, 0);
		}
	}
}
