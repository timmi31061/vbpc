<?php
	require_once('Database.class.php');
	require_once('Util.class.php');

$_POST['username'] = "timmi31061";
$_POST['userid'] = "9310";

	// Check for arguments
	$requiredArgs = array('username', 'userid');
	for($i = 0, $j = count($requiredArgs); $i < $j; $i++) {
		if(isset($_POST[$requiredArgs[$i]]) == FALSE) {
			echo '{"sucess": false, "error": "Invalid parameters.", "code": "ERR_REQ_INV"}';
			exit;
		}
	}

	// May the user be registered?
	$st = Database::Connection()->prepare("SELECT * FROM allowedUsers WHERE username = ?");
	$st->bindParam(1, $_POST['username']);
	$st->execute();
	$st->setFetchMode(PDO::FETCH_ASSOC);
	$row = $st->fetch();
	if($row === false) {
		echo '{"sucess": false, "error": "User not allowed.", "code": "ERR_USER_DIS"}';
		exit;
	}

	// It is a valid user, is a password required?
	$st = Database::Connection()->prepare("SELECT * FROM registeredUsers WHERE username = ?");
	$st->bindParam(1, $_POST['username']);
	$st->execute();
	$st->setFetchMode(PDO::FETCH_ASSOC);
	$row = $st->fetch();
	if($row !== false && isset($_POST['password']) == false) {
		echo '{"sucess": false, "error": "Password required.", "code": "ERR_PASSWD_REQ"}';
		exit;
	} else 	if($row !== false && Util::hash($_POST['password'], $row['salt']) == $row['password']) {
		echo '{"sucess": false, "error": "Password invalid.", "code": "ERR_PASSWD_INV"}';
		exit;
	} 

	// Return token
	if($row !== false) {
		$st = Database::Connection()->prepare("SELECT * FROM registeredUsers WHERE username = ?");
		$st->bindParam(1, $_POST['username']);
		$st->execute();
		$row = $st->fetch();
		echo "";
	}
	$token = Util::generateSalt();
	$st = Database::Connection()->prepare("INSERT INTO registeredUsers (vbpId, username, token, salt) VALUES (?, ?, ?, ?)");
	$st->bindParam(1, $_POST['userid']);
	$st->bindParam(2, $_POST['username']);
	$st->bindParam(3, $token);
	$st->bindParam(4, Util::generateSalt())
	$st->execute();
	
	echo '{"success": true, "token": "'.$token.'"}';
	echo "\n";
