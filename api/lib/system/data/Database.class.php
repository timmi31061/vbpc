<?php
	class Database {
		public static $host   = "localhost";
		public static $database = "vbpc";
		public static $user   = "root";
		public static $passwd = "root";

		private static $DBH;
		private static $connected = false;

		public static function Connect() {
			if(!self::$connected) {
				self::$connected = true;
				self::$DBH = new PDO("mysql:host=". self::$host . ";dbname=" . self::$database, self::$user, self::$passwd);
				self::$DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
			}
		}

		public static function Connection() {
			self::Connect();
			return self::$DBH;
		}

		public static function Close() {
			self::$DBH = null;
		}
	}
