<?php
namespace vbpc\system;
use wcf\system\exception\SystemException;
use wcf\system\exception\LoggedException;
use wcf\system\exception\IPrintableException;

//exit;
//throw new SystemException("Error Processing Request");

//new VBPC();
/*
// Set exception handler
set_exception_handler(array('vbpc\system\VBPC', 'handleException'));

// Set php error handler
set_error_handler(array('vbpc\system\VBPC', 'handleError'), E_ALL);

// Set autoloader
spl_autoload_register(array('vbpc\system\VBPC', 'autoload'));*/

class VBPC {
	/**
	 * Includes the required classes automatically.
	 * 
	 * @param	string		$className
	 */
	public static final function autoload($className) {
		$namespaces = explode('\\', $className);
		if(count($namespaces) > 1) {
			$prefix = array_shift($namespaces);
			if($prefix === '') {
				$prefix = array_shift($namespaces);
			}
			if($prefix != 'vbpc') {
				throw new SystemException('Invalid namespace.');
			}

			$classPath = VBPC_DIR . 'lib/' . implode('/', $namespaces) . '.class.php';
			if(file_exists($classPath)) {
				require_once($classPath);
			}
		}
	}

	/**
	 * Prints an exception.
	 * 
	 * @param	\Exception	$e
	 */
	public static final function handleException($e) {
		if(defined('VBPC_DEBUG_MODE') && VBPC_DEBUG_MODE) {
			if($e instanceof LoggedException) {
				echo "<pre>".htmlentities($e->getLogMessage());
			} else {
				echo 'Unhandled '.get_class($e)."\n".
					 'Message: '.$e->getMessage()."\n".
					 'File: '.$e->getFile().' ('.$e->getLine().")\n".
					 "Stacktrace: \n  ".implode("\n  ", explode("\n", $e->getTraceAsString()))."\n";
			}
		} else {
			if($e instanceof LoggedException) {
				echo '{"success":false, "code":"ERR_EXCEPTION", "id":"'.$e->getExceptionID().'"}';
			} else if($e instanceof PrintableException) {
				echo '{"success":false, "code":"ERR_EXCEPTION", "message":"'.$e->getMessage().'"}';
			} else {
				echo '{"success":false, "code":"ERR_EXCEPTION"}';
			}
		}
		exit;
	}
	
	/**
	 * Catches php errors and throws a system exception instead.
	 * 
	 * @param	integer		$errorNo
	 * @param	string		$message
	 * @param	string		$filename
	 * @param	integer		$lineNo
	 */
	public static final function handleError($errorNo, $message, $filename, $lineNo) {
		if (error_reporting() != 0) {
			$type = 'error';
			switch ($errorNo) {
				case 2: $type = 'warning';
					break;
				case 8: $type = 'notice';
					break;
			}
			
			throw new SystemException('PHP '.$type.' in file '.$filename.' ('.$lineNo.'): '.$message, 0);
		}
	}
}