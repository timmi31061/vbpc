<?php
namespace vbpc\system;
use wcf\system\exception\SystemException;
use wcf\system\SingletonFactory;
use wcf\system\WCF;
use wcf\util\JSON;

class Request extends SingletonFactory {
	protected $action;
	protected $actionClass;

	protected function init() {
		// example.com/index.php/Test/Action?key=value -> Test/Action
		$path = substr($_SERVER['PATH_INFO'], 1);

		$pathParts = explode('/', $path);
		$pathParts[count($pathParts) - 1] = ucfirst($pathParts[count($pathParts) - 1]);
		$this->action = implode('\\', $pathParts);
		$this->actionClass = 'vbpc\\action\\'.$this->action.'Action';		
	}

	public function execute() {
		if(class_exists($this->actionClass)) {
			$action = new $this->actionClass();

			$action->setParameters($_REQUEST);

			if(isset($_REQUEST['action'])) {
				$action->setAction($_REQUEST['action']);
			} else {
				$action->setAction('action');
			}
			$action->validate();
			$action->execute();
		} else {
			throw new SystemException("Could not execute action '".$this->actionClass."': Handler not found.");
		}
	}
}
