<?php
namespace vbpc\util;
use wcf\system\exception\SystemException;

/**
 * Provides functions to work with password hashes.
 *
 * @author	Alexander Ebert
 * @author	Tim Schiewe
 * @package	de.tforge.vbpc
 * @subpackage	util
 * @category	VB-Paradise Comments
 */
final class PasswordUtil {
	/**
	 * concated list of valid blowfish salt characters
	 * @var	string
	 */
	private static $blowfishCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789./';

	/**
	 * blowfish cost factor
	 * @var	string
	 */
	const BCRYPT_COST = '08';
	
	/**
	 * blowfish encryption type
	 * @var	string
	 */
	const BCRYPT_TYPE = '2a';

	/**
	 * Validates password against stored hash.
	 * 
	 * @param	string		$password
	 * @param	string		$dbHash
	 * @return	boolean
	 */
	public static function checkPassword($password, $dbHash) {
		return PasswordUtil::secureCompare($dbHash, self::getDoubleSaltedHash($password, $dbHash));
	}

	/**
	 * Compares two password hashes. This function is protected against timing attacks.
	 * 
	 * @see		http://codahale.com/a-lesson-in-timing-attacks/
	 * 
	 * @param	string		$hash1
	 * @param	string		$hash2
	 * @return	boolean
	 */
	public static function secureCompare($hash1, $hash2) {
		$hash1 = (string)$hash1;
		$hash2 = (string)$hash2;
		
		if (strlen($hash1) !== strlen($hash2)) {
			return false;
		}
		
		$result = 0;
		for ($i = 0, $length = strlen($hash1); $i < $length; $i++) {
			$result |= ord($hash1[$i]) ^ ord($hash2[$i]);
		}
		
		return ($result === 0);
	}
	
	/**
	 * Returns a double salted bcrypt hash.
	 * 
	 * @param	string		$password
	 * @param	string		$salt
	 * @return	string
	 */
	public static function getDoubleSaltedHash($password, $salt = null) {
		if ($salt === null) {
			$salt = self::getRandomSalt();
		}
		
		return self::getSaltedHash(self::getSaltedHash($password, $salt), $salt);
	}

	/**
	 * Returns a simple salted bcrypt hash.
	 * 
	 * @param	string		$password
	 * @param	string		$salt
	 * @return	string
	 */
	public static function getSaltedHash($password, $salt = null) {
		if ($salt === null) {
			$salt = self::getRandomSalt();
		}
		
		return crypt($password, $salt);
	}
	
	/**
	 * Returns a random blowfish salt.
	 * 
	 * @return	string
	 */
	public static function getRandomSalt() {
		$salt = '';
		
		for ($i = 0, $maxIndex = (strlen(self::$blowfishCharacters) - 1); $i < 22; $i++) {
			$salt .= self::$blowfishCharacters[self::secureRandomNumber(0, $maxIndex)];
		}
		
		$salt = mb_substr($salt, 0, 22);
		return '$' . self::BCRYPT_TYPE . '$' . self::BCRYPT_COST . '$' . $salt;
	}

	/**
	 * Generates secure random numbers using OpenSSL.
	 * 
	 * This method forces mt_rand() if PHP versions below 5.4.0 are used due to a bug
	 * causing up to 15 seconds delay until the bytes are returned.
	 * 
	 * @see		http://de1.php.net/manual/en/function.openssl-random-pseudo-bytes.php#104322
	 * @param	integer		$min
	 * @param	integer		$max
	 * @return	integer
	 */
	public static function secureRandomNumber($min, $max) {
		$range = $max - $min;
		if ($range == 0) {
			// not random
			throw new SystemException("Cannot generate a secure random number, min and max are the same");
		}
		
		// fallback to mt_rand() if OpenSSL is not available
		if (version_compare(PHP_VERSION, '5.4.0-dev', '<') || !function_exists('openssl_random_pseudo_bytes')) {
			return mt_rand($min, $max);
		}
		
		$log = log($range, 2);
		$bytes = (int) ($log / 8) + 1; // length in bytes
		$bits = (int) $log + 1; // length in bits
		$filter = (int) (1 << $bits) - 1; // set all lower bits to 1
		do {
			$rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes, $s)));
			$rnd = $rnd & $filter; // discard irrelevant bits
		}
		while ($rnd >= $range);
		
		return $min + $rnd;
	}
}
