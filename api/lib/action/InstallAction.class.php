<?php
namespace vbpc\action;
use vbpc\data\invitation\Invitation;
use vbpc\data\user\User;
use vbpc\data\user\UserEditor;
use vbpc\util\PasswordUtil;
use wcf\system\database\DatabaseException;
use wcf\util\JSON;
use wcf\util\StringUtil;

class InstallAction extends AbstractAction {

	/**
	 * validates the 'checkRegister'-Action
	 */
	public function validateCheckRegister() {
		$this->readString('username');
	}

	/**
	 * executes the checkRegisterAction
	 */
	public function checkRegister() {
		// Check invitation
		$invitation = Invitation::getInvitationByUsername($this->parameters['username']);
		if($invitation->invitationID == 0) {
			echo JSON::encode(array(
				'result' => 'USER_NOT_INVITED'
			));
			return;
		}

		// Check if user is registered
		$user = User::getUserByUsername($this->parameters['username']);
		if($user->userID) {
			echo JSON::encode(array(
				'result' => 'USER_IS_REGISTERED'
			));
			return;
		}

		// User is not registered
		echo JSON::encode(array(
			'result' => 'USER_NOT_REGISTERED'
		));
	}

	/**
	 * validates the 'register'-Action
	 */
	public function validateRegister() {
		$this->readInteger('userID');
		$this->readString('username');
		//$this->readString('password');

		// Check invitation
		$invitation = Invitation::getInvitationByUsername($this->parameters['username']);
		if($invitation->invitationID == 0) {
			throw new SystemException(JSON::encode(array(
				'result' => 'USER_NOT_INVITED'
			)));
		}

		// Check if user is registered
		$user = User::getUserByUsername($this->parameters['username']);
		if($user->userID) {
			throw new SystemException(JSON::encode(array(
				'result' => 'USER_IS_REGISTERED'
			)));
		}
	}

	/**
	 * executes the 'register'-Action
	 */
	public function register() {
		$token = '';
		while(true) {
			$token = StringUtil::getRandomID();
			try {
				UserEditor::create(array(
					'userID' => $this->parameters['userID'],
					'username' => $this->parameters['username'],
					'password' => PasswordUtil::getDoubleSaltedHash('password'),
					'token' => $token
				));
				break;
			} catch(DatabaseException $d) {
			}
		}

		echo JSON::encode(array(
			'token' => $token
		));
	}

	/**
	 * validates the 'token'-Action
	 */
	public function validateToken() {
		$this->readString('username');
		$this->readString('password');

		$user = User::getUserByUsername($this->parameters['username']);
		if($user->userID == 0) {
			// User is not registered
			throw new SystemException(JSON::encode(array(
				'result' => 'USER_NOT_REGISTERED'
			)));
		}

		if($user->checkPassword($this->parameters['parameters'])) {
			// User is not registered
			throw new SystemException(JSON::encode(array(
				'result' => 'USER_NOT_REGISTERED'
			)));
		}

		$this->parameters['user'] = $user;
	}


	/**
	 * executes the 'token'-Action
	 */
	public function token() {
		echo JSON::encode(array(
			'token' => $this->parameters['user']->token
		));
	}
}