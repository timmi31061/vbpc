<?php
namespace vbpc\action;
use vbpc\data\comment\CommentEditor;
use vbpc\data\mention\MentionEditor;
use vbpc\data\user\User;
use vbpc\data\user\UserEditor;
use wcf\util\JSON;

class PostAction extends AuthedAction {
	public function validateAction() {
		$this->readInteger('postID');
		$this->readString('postTitle');
		$this->readString('message');
	}

	public function action() {
		// Store comment
		$comment = CommentEditor::create(array(
			'userID' => $this->user->userID,
			'postID' => $this->parameters['postID'],
			'postTitle' => $this->parameters['postTitle'],
			'message' => $this->parameters['message']
		));

		// Update user's comment count
		$userEditor = new UserEditor($this->user);
		$userEditor->update(array(
			'comments' => intval($this->user->comments) + 1
		));

		// Parse mentions
		$mentionedUserInfo = array();
		preg_match_all('/@([^\s]+)/', $comment->message, $mentions);
		$mentions = $mentions[1];

		// Store mentions
		foreach ($mentions as $mention) {
			$mentionedUser = User::getUserByUsername($mention);

			MentionEditor::create(array(
				'commentID' => $comment->commentID,
				'userID' => $mentionedUser->userID
			));
		}

		echo '{}';
	}
}
