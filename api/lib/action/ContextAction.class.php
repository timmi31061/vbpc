<?php
namespace vbpc\action;
use vbpc\data\mention\MentionList;
use wcf\util\JSON;

class ContextAction extends AuthedAction {
	public function validateAction() {
	}

	public function action() {
		// Read mention count
		$mentions = new MentionList();
		$mentions->getConditionBuilder()->add('userID = ?', array($this->user->userID));
		$mentions->getConditionBuilder()->add('isRead = 0');

		echo JSON::encode(array(
			'isAdmin' => (bool)($this->user->isAdmin),
			'unreadMentions' => intval($mentions->countObjects())
		));
	}
}
