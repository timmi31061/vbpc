<?php
namespace vbpc\action;
use vbpc\data\comment\Comment;
use vbpc\data\comment\CommentEditor;
use wcf\system\exception\UserInputException;
use wcf\util\JSON;

class DeleteAction extends AuthedAction {
	public function validateAction() {
		$this->readInteger('commentID');
	}

	public function action() {
		// Read comment
		$comment = new Comment($this->parameters['commentID']);

		// Check permission
		if($comment->userID == $this->user->userID || $this->user->isAdmin) {
			$commentEditor = new CommentEditor($comment);
			$commentEditor->delete();
		} else {
			throw new UserInputException('commentID');
		}

		echo '{}';
	}
}
