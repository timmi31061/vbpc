<?php
namespace vbpc\action;
use vbpc\data\comment\Comment;
use vbpc\data\comment\CommentList;
use vbpc\data\mention\Mention;
use vbpc\data\mention\MentionEditor;
use vbpc\data\mention\MentionList;
use vbpc\data\user\User;
use vbpc\util\PasswordUtil;
use wcf\system\database\DatabaseException;
use wcf\util\JSON;
use wcf\util\StringUtil;

class CommentsAction extends AuthedAction {
	public function validateAction() {
		$this->readString('postIDs', true);
		$this->readBoolean('readMentions', true);
	}

	public function action() {
		// Split post IDs
		$postIDs = null;
		if (empty($this->parameters['postIDs']) == false) {
			$postIDs = explode(',', $this->parameters['postIDs']);
		}

		// Create result array
		$result = array();

		// Fetch last comments
		if ($postIDs == null && $this->parameters['readMentions'] == false) {
			$result = $this->readLastComments();

		// Fetch last mentions
		} else if ($postIDs == null && $this->parameters['readMentions']) {
			$result = $this->readLastMentions();

		// Fetch post's comments
		} else if ($postIDs != null && $this->parameters['readMentions'] == false) {
			$result = $this->readComments($postIDs);
		}

		if (count($result)) {
			echo JSON::encode($result);
		} else {
			echo '{}';
		}


		return;




		// Holzhammer-Methode
		while(true) {
			try {
				UserEditor::create(array(
					'token' => StringUtil::getRandomID()
				));
				break;
			} catch(DatabaseException $d) {
			}
		}

		
		$hash = '$2a$08$ux5eWeWs0EQbj8Ou2hBzROSpuRDR8OI9SCKnDmyhn/RUIYI6JrNZu';
		var_dump(StringUtil::getRandomID());
		var_dump(PasswordUtil::checkPassword('Hallo', $hash));

		var_dump(PasswordUtil::getDoubleSaltedHash('Hallo', $hash));
	}


	private function readLastComments() {
		$result = array();

		// Read comments
		$comments = new CommentList();
		$comments->sqlLimit = 50;
		$comments->readObjects();

		// Fetch data for client
		foreach ($comments as $comment) {
			$result[] = $this->buildComment($comment);
		}

		return $result;
	}

	private function readLastMentions() {
		$result = array();

		// Read mentions
		$mentions = new MentionList();
		$mentions->getConditionBuilder()->add('userID = ?', array($this->user->userID));
		$mentions->getConditionBuilder()->add('isRead = 0');
		$mentions->readObjects();

		foreach ($mentions as $mention) {
			$mentionedComment = new Comment($mention->commentID);
			$result[] = $this->buildComment($mentionedComment);
		}

		return $result;
	}

	private function readComments($postIDs) {
		$result = array();

		// Loop over all posts
		foreach ($postIDs as $post) {
			// Result
			$resultComments = array();

			// Read comments
			$comments = new CommentList();
			$comments->getConditionBuilder()->add('postID = ?', array(intval($post)));
			$comments->readObjects();

			// Only add comments if the are any
			if ($comments->count()) {
				// Fetch data for client
				foreach ($comments as $comment) {
					$resultComments[] = $this->buildComment($comment);

					// Set mentions as read
					$mentions = new MentionList();
					$mentions->getConditionBuilder()->add('userID = ?', array($this->user->userID));
					$mentions->getConditionBuilder()->add('commentID = ?', array($comment->commentID));
					$mentions->getConditionBuilder()->add('isRead = 0');
					$mentions->readObjects();

					foreach($mentions as $mention) {
						$mentionEditor = new MentionEditor($mention);
						$mentionEditor->update(array(
							'isRead' => 1
						));
					}
				}

				// Store comments
				$result[$post] = $resultComments;
			}
		}

		return $result;
	}

	private function buildComment($comment) {
		// Read user info
		$user = new User($comment->userID);

		// Read mentions
		$mentions = new MentionList();
		$mentions->getConditionBuilder()->add('commentID = ?', array($comment->getObjectID()));
		$mentions->readObjects();

		// Read mentioned user info
		$mentionedUserInfo = array();
		foreach ($mentions as $mention) {
			$mentionedUser = new User($mention->userID);
			$mentionedUserInfo[] = array(
				'userID' => $mentionedUser->userID,
				'username' => $mentionedUser->username
			);
		}

		// Build comment
		return array(
			'commentID' => $comment->commentID,
			'timestamp' => strtotime($comment->time),
			'postTitle' => $comment->postTitle,
			'postID' => $comment->postID,
			'message' => nl2br(StringUtil::encodeHTML($comment->message)),
			'user' => array(
				'userID' => $user->userID,
				'username' => $user->username
			),
			'mentionedUserInfo' => $mentionedUserInfo
		);
	}
}
