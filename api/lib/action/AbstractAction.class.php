<?php
namespace vbpc\action;
use wcf\system\exception\SystemException;
use wcf\system\exception\UserInputException;

/**
 * AbstractAction handles actions.
 * 
 * @author	Tim Schiewe
 */
abstract class AbstractAction {
	const TYPE_INTEGER = 1;
	const TYPE_STRING = 2;
	const TYPE_BOOLEAN = 3;

	/**
	 *	Array of validated parameters
	 *  @var array<string, mixed>
	 */
	protected $parameters = array();

	/**
	 *  Pending Action
	 *  @var string
	 */
	protected $action;

	/**
	 *	Array of available parameters
	 *  @var array<string, mixed>
	 */
	private $params;

	public function setParameters($params) {
		$this->params = $params;
	}

	public function setAction($action) {
		$this->action = $action;
	}

	public function validate() {
		$validateMethod = 'validate'.ucfirst($this->action);
		if(method_exists($this, $validateMethod) == false) {
			throw new SystemException('Unknown action "'.$this->action.'" for '.get_class($this));
		}

		$valid = false;
		$valid = call_user_func(array($this, $validateMethod));
	}

	public function execute() {
		$executeMethod = $this->action;
		if(method_exists($this, $executeMethod) == false) {
			throw new SystemException('Unknown action "'.$this->action.'" for '.get_class($this));
		}
		call_user_func(array($this, $executeMethod));
	}



	protected final function readInteger($variableName, $allowEmpty = false) {
		$this->readValue($variableName, $allowEmpty, self::TYPE_INTEGER);
	}

	protected final function readBoolean($variableName, $allowEmpty = false) {
		$this->readValue($variableName, $allowEmpty, self::TYPE_BOOLEAN);
	}

	protected final function readString($variableName, $allowEmpty = false) {
		$this->readValue($variableName, $allowEmpty, self::TYPE_STRING);
	}

	protected final function readValue($variableName, $allowEmpty, $type) {

		switch ($type) {
			case self::TYPE_INTEGER:
				if (!isset($this->params[$variableName])) {
					if ($allowEmpty) {
						$this->parameters[$variableName] = 0;
					}
					else {
						throw new UserInputException($variableName);
					}
				}
				else {
					$this->parameters[$variableName] = intval($this->params[$variableName]);
					if (!$allowEmpty && !$this->parameters[$variableName]) {
						throw new UserInputException($variableName);
					}
				}
			break;

			case self::TYPE_BOOLEAN:
				if (!isset($this->params[$variableName])) {
					if ($allowEmpty) {
						$this->parameters[$variableName] = false;
					}
					else {
						throw new UserInputException($variableName);
					}
				}
				else {
					if (is_numeric($this->params[$variableName])) {
						$n = intval($this->params[$variableName]);
						if($n == 1) {
							$this->parameters[$variableName] = true;
						} else if($n == 0) {
							$this->parameters[$variableName] = false;
						} else {
							throw new UserInputException($variableName);
						}
					}
					else {
						if($this->params[$variableName] == 'true') {
							$this->parameters[$variableName] = true;
						} else if($this->params[$variableName] == 'false') {
							$this->parameters[$variableName] = false;
						} else {
							throw new UserInputException($variableName);
						}
					}
					if (!$allowEmpty && !$this->parameters[$variableName]) {
						throw new UserInputException($variableName);
					}
				}
			break;

			case self::TYPE_STRING:
				if (!isset($this->params[$variableName])) {
					if ($allowEmpty) {
						$this->parameters[$variableName] = '';
					}
					else {
						throw new UserInputException($variableName);
					}
				}
				else {
					$this->parameters[$variableName] = trim($this->params[$variableName]);
					if (!$allowEmpty && empty($this->parameters[$variableName])) {
						throw new UserInputException($variableName);
					}
				}
			break;
		}
	}
}
