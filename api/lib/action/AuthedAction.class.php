<?php
namespace vbpc\action;
use vbpc\data\user\User;
use wcf\system\exception\UserInputException;
use wcf\system\exception\SystemException;

abstract class AuthedAction extends AbstractAction {
	/**
	 * authenticated user
	 * @var vbpc\data\user\User
	 */
	protected $user;

	/**
	 * Validates user and parameters
	 */
	public function validate() {
			//throw new SystemException('token');
		$this->readString('token');

		$this->user = User::getUserByToken($this->parameters['token']);
		if(!$this->user->userID) {
			throw new UserInputException('token');
		}
		parent::validate();
	}
}
