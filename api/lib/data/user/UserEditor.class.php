<?php
namespace vbpc\data\user;
use wcf\data\DatabaseObjectEditor;
 
class UserEditor extends DatabaseObjectEditor {
	/**
	 * @see	\wcf\data\DatabaseObjectDecorator::$baseClass
	 */
	protected static $baseClass = 'vbpc\data\user\User';
}