<?php
namespace vbpc\data\user;
use vbpc\util\PasswordUtil;
use wcf\data\DatabaseObject;
use wcf\system\WCF;

class User extends DatabaseObject {
	/**
	 * @see	\wcf\data\DatabaseObject::$databaseTableName
	 * @var string
	 */
	protected static $databaseTableName = 'user';
	
	/**
	 * @see	\wcf\data\DatabaseObject::$databaseIndexName
	 * @var string
	 */
	protected static $databaseTableIndexName = 'userID';
	
	/**
	 * Returns the user by the given username.
	 * 
	 * @param	string		$username
	 * @return	\vbpc\data\user\User
	 */
	public static function getUserByUsername($username) {
		$sql = "SELECT	*
			FROM	".self::getDatabaseTableName()."
			WHERE	username = ?";
		$statement = WCF::getDB()->prepareStatement($sql);
		$statement->execute(array($username));
		$row = $statement->fetchArray();
		if (!$row) $row = array();
		
		return new User(null, $row);
	}

	/**
	 * Returns the user with the given token.
	 * 
	 * @param	string		$token
	 * @return	\vbpc\data\user\User
	 */
	public static function getUserByToken($token) {
		$sql = "SELECT	*
			FROM	".self::getDatabaseTableName()."
			WHERE	token = ?";
		$statement = WCF::getDB()->prepareStatement($sql);
		$statement->execute(array($token));
		$row = $statement->fetchArray();
		if (!$row) $row = array();
		
		return new User(null, $row);
	}

	/**
	 * Checks the password.
	 * 
	 * @param	string		$password
	 * @return	boolean
	 */
	public static function checkPassword($password) {
		return PasswordUtil::checkPassword($password, $this->password);
	}

	/**
	 * @see	\wcf\data\IStorableObject::getDatabaseTableName()
	 */
	public static function getDatabaseTableName() {
		return 'vbpc'.WCF_N.'_'.static::$databaseTableName;
	}
}
