<?php
namespace vbpc\data\user;
use wcf\data\DatabaseObjectList;

class UserList extends DatabaseObjectList {
	/**
	 * @see	\wcf\data\DatabaseObjectList::$className
	 */
	public $className = 'vbpc\data\user\User';
}
