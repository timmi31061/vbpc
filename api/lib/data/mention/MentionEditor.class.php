<?php
namespace vbpc\data\mention;
use wcf\data\DatabaseObjectEditor;
 
class MentionEditor extends DatabaseObjectEditor {
	/**
	 * @see	\wcf\data\DatabaseObjectDecorator::$baseClass
	 */
	protected static $baseClass = 'vbpc\data\mention\Mention';
}