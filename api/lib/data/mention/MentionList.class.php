<?php
namespace vbpc\data\mention;
use wcf\data\DatabaseObjectList;

class MentionList extends DatabaseObjectList {
	/**
	 * @see	\wcf\data\DatabaseObjectList::$className
	 */
	public $className = 'vbpc\data\mention\Mention';
}
