<?php
namespace vbpc\data\mention;
use wcf\data\DatabaseObject;

class Mention extends DatabaseObject {
	/**
	 * @see	\wcf\data\DatabaseObject::$databaseTableName
	 * @var string
	 */
	protected static $databaseTableName = 'mention';
	
	/**
	 * @see	\wcf\data\DatabaseObject::$databaseIndexName
	 * @var string
	 */
	protected static $databaseTableIndexName = 'mentionID';

	/**
	 * @see	\wcf\data\IStorableObject::getDatabaseTableName()
	 */
	public static function getDatabaseTableName() {
		return 'vbpc'.WCF_N.'_'.static::$databaseTableName;
	}
}
