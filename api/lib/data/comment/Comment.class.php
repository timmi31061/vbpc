<?php
namespace vbpc\data\comment;
use wcf\data\DatabaseObject;

class Comment extends DatabaseObject {
	/**
	 * @see	\wcf\data\DatabaseObject::$databaseTableName
	 * @var string
	 */
	protected static $databaseTableName = 'comment';
	
	/**
	 * @see	\wcf\data\DatabaseObject::$databaseIndexName
	 * @var string
	 */
	protected static $databaseTableIndexName = 'commentID';

	/**
	 * @see	\wcf\data\IStorableObject::getDatabaseTableName()
	 */
	public static function getDatabaseTableName() {
		return 'vbpc'.WCF_N.'_'.static::$databaseTableName;
	}
}
