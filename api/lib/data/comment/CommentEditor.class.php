<?php
namespace vbpc\data\comment;
use wcf\data\DatabaseObjectEditor;
 
class CommentEditor extends DatabaseObjectEditor {
	/**
	 * @see	\wcf\data\DatabaseObjectDecorator::$baseClass
	 */
	protected static $baseClass = 'vbpc\data\comment\Comment';
}