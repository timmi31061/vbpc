<?php
namespace vbpc\data\comment;
use wcf\data\DatabaseObjectList;

class CommentList extends DatabaseObjectList {
	/**
	 * @see	\wcf\data\DatabaseObjectList::$className
	 */
	public $className = 'vbpc\data\comment\Comment';
}
