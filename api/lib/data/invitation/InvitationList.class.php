<?php
namespace vbpc\data\invitation;
use wcf\data\DatabaseObjectList;

class InvitationList extends DatabaseObjectList {
	/**
	 * @see	\wcf\data\DatabaseObjectList::$className
	 */
	public $className = 'vbpc\data\invitation\Invitation';
}
