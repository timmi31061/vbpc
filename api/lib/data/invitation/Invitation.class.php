<?php
namespace vbpc\data\invitation;
use wcf\data\DatabaseObject;
use wcf\system\WCF;

class Invitation extends DatabaseObject {
	/**
	 * @see	\wcf\data\DatabaseObject::$databaseTableName
	 * @var string
	 */
	protected static $databaseTableName = 'invitation';
	
	/**
	 * @see	\wcf\data\DatabaseObject::$databaseIndexName
	 * @var string
	 */
	protected static $databaseTableIndexName = 'invitationID';
	
	/**
	 * Returns the invitation by the given username.
	 * 
	 * @param	string		$username
	 * @return	\vbpc\data\user\User
	 */
	public static function getInvitationByUsername($username) {
		$sql = "SELECT	*
			FROM	".self::getDatabaseTableName()."
			WHERE	username = ?";
		$statement = WCF::getDB()->prepareStatement($sql);
		$statement->execute(array($username));
		$row = $statement->fetchArray();
		if (!$row) $row = array();
		
		return new Invitation(null, $row);
	}

	/**
	 * @see	\wcf\data\IStorableObject::getDatabaseTableName()
	 */
	public static function getDatabaseTableName() {
		return 'vbpc'.WCF_N.'_'.static::$databaseTableName;
	}
}
