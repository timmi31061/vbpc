<?php
namespace vbpc\data\invitation;
use wcf\data\DatabaseObjectEditor;
 
class InvitationEditor extends DatabaseObjectEditor {
	/**
	 * @see	\wcf\data\DatabaseObjectDecorator::$baseClass
	 */
	protected static $baseClass = 'vbpc\data\invitation\Invitation';
}