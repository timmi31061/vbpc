<?php
use vbpc\system\Request;

//define('WCF_DEBUG_MODE', true);

// Define the VBPC-root-dir
define('VBPC_DIR', dirname(__FILE__).'/');
define('WCF_DIR', dirname(__FILE__).'/wcf/');

// Initialize cores
require_once(WCF_DIR . 'lib/system/WCF.class.php');
new wcf\system\WCF();

require_once(VBPC_DIR . 'lib/system/VBPC.class.php');
//new vbpc\system\VBPC();

header('Access-Control-Allow-Origin: *');
Request::getInstance()->execute();

/*
$reg = new RegisteredUser();

$zeug = substr($_SERVER['PHP_SELF'], strlen($_SERVER['SCRIPT_NAME']));

echo '<pre>';
echo $zeug . "\n";
print_r($_ENV);
print_r($_SERVER);
print_r($_REQUEST);*/
