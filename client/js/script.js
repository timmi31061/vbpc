var Config = (function () {
    function Config() {
    }
    Config.CurrentVersion = '3.0.0';

    Config.ProtocolPrefix = document.location.protocol + '//';

    Config.IsHttps = document.location.protocol == 'https:';

    Config.TargetHostName = 'www.vb-paradise.de';
    Config.TargetBaseUrl = Config.ProtocolPrefix + Config.TargetHostName + '/index.php/';
    Config.ProfileUrlTemplate = Config.TargetBaseUrl + 'User/';

    Config.ServiceHostName = 'xmpp.tforge.de';
    Config.ServiceBaseUrl = Config.ProtocolPrefix + Config.ServiceHostName + '/';
    Config.ApiBaseUrl = Config.ServiceBaseUrl + 'api/';
    Config.ClientUrl = Config.ServiceBaseUrl + 'client/';
    Config.StylesheetUrl = Config.ClientUrl + 'css/style.css';

    Config.VbpcTabUrl = '/index.php/MembersList/?vbpc=vbpc';
    Config.VbpcTabComments = '/index.php/MembersList/?vbpc=comments';
    Config.VbpcTabMentions = '/index.php/MembersList/?vbpc=mentions';

    Config.CommentMaxLength = 32768;
    return Config;
})();
/// <reference path="../Config.ts"/>
var Urls = (function () {
    function Urls() {
    }
    Urls.Comments = Config.ApiBaseUrl + 'index.php/Comments';
    Urls.Context = Config.ApiBaseUrl + 'index.php/Context';
    Urls.Post = Config.ApiBaseUrl + 'index.php/Post';
    Urls.Delete = Config.ApiBaseUrl + 'index.php/Delete';
    Urls.Install = Config.ApiBaseUrl + 'index.php/Install';
    return Urls;
})();
var Context = (function () {
    function Context() {
    }
    Context.unreadMentions = 0;
    Context.isAdmin = false;
    Context.threadTitle = "";
    return Context;
})();
/// <reference path="Urls.ts"/>
/// <reference path="../Client.ts"/>
/// <reference path="../Context.ts"/>
/// <reference path="../lib/WCF.User.d.ts"/>
var API = (function () {
    function API() {
    }
    API.request = function (url, data, callback) {
        if (data == null) {
            data = new Object();
        }
        data['token'] = localStorage.getItem('vbpc_token');

        $.post(url, data, callback, 'json');
    };

    API.postComment = function (postID, message, callback) {
        var data = new Object();
        data['postID'] = postID;
        data['postTitle'] = Context.threadTitle;
        data['message'] = message;

        API.request(Urls.Post, data, callback);
    };

    API.deleteComment = function (commentID, callback) {
        var data = new Object();
        data['commentID'] = commentID;

        API.request(Urls.Delete, data, callback);
    };

    API.getComments = function (postIDs, callback) {
        var data = new Object();
        if (postIDs != null) {
            data['postIDs'] = postIDs.join(',');
        }

        API.request(Urls.Comments, data, callback);
    };

    API.getMentions = function (callback) {
        var data = new Object();
        data['readMentions'] = true;

        API.request(Urls.Comments, data, callback);
    };

    API.getContext = function (callback) {
        var data = new Object();
        API.request(Urls.Context, data, callback);
    };

    API.checkAccount = function (callback) {
        var data = new Object();
        data['action'] = 'checkRegister';
        data['username'] = WCF.User.username;

        API.request(Urls.Install, data, callback);
    };

    API.register = function (callback) {
        var data = new Object();
        data['action'] = 'register';
        data['userID'] = WCF.User.userID;
        data['username'] = WCF.User.username;

        API.request(Urls.Install, data, callback);
    };

    API.checkToken = function (token, callback) {
        var data = new Object();
        data['token'] = token;

        $.post(Urls.Context, data, function (resp) {
            try  {
                JSON.parse(resp);
                callback(true);
            } catch (e) {
                callback(false);
            }
        }, 'text');
    };
    return API;
})();
/// <reference path="../Config.ts"/>
var User = (function () {
    function User(obj) {
        if (typeof obj === "undefined") { obj = null; }
        if (obj == null) {
            return;
        }

        for (var prop in obj) {
            this[prop] = obj[prop];
        }
    }
    User.prototype.getProfileUrl = function () {
        return Config.ProfileUrlTemplate + this.userID.toString() + "-" + this.username;
    };

    User.prototype.toString = function () {
        return this.username;
    };
    return User;
})();
/// <reference path="../Config.ts"/>
/// <reference path="../Context.ts"/>
/// <reference path="User.ts"/>
var VbpcComment = (function () {
    function VbpcComment(obj) {
        if (typeof obj === "undefined") { obj = null; }
        if (obj == null) {
            return;
        }

        for (var prop in obj) {
            this[prop] = obj[prop];
        }

        // Create own objects from the flat JSON-objects
        this.user = new User(this.user);

        for (var i = 0; i < this.mentionedUserInfo.length; i++) {
            this.mentionedUserInfo[i] = new User(this.mentionedUserInfo[i]);
        }
    }
    VbpcComment.prototype.getProfileUrl = function () {
        return this.user.getProfileUrl();
    };

    VbpcComment.prototype.toString = function () {
        var regexLink = /(https?:\/\/[^ ]+)/g;
        var regexBbCodes = new Array(/\[b\](.*?)\[\/b\]/gi, /\[i\](.*?)\[\/i\]/gi, /\[u\](.*?)\[\/u\]/gi, /\[s\](.*?)\[\/s\]/gi);
        var bbCodeHtml = new Array("<strong>$1</strong>", "<em>$1</em>", "<span style=\"text-decoration: underline\">$1</span>", "<span style=\"text-decoration: line-through\">$1</span>");

        var outputHtml = this.message;

        // Parse Links
        outputHtml = outputHtml.replace(regexLink, "<a href=\"$1\" title=\"\">$1</a>");

        // Parse mentions
        // Are mention user infos defined?
        if (this.mentionedUserInfo != null || this.mentionedUserInfo.length > 0) {
            for (var i = 0; i < this.mentionedUserInfo.length; i++) {
                var current = this.mentionedUserInfo[i];
                outputHtml = outputHtml.replace('@' + current.username, '<a href="' + current.getProfileUrl() + '">@' + current.username + '</a>');
            }
        }

        for (var i = 0; i < regexBbCodes.length; i++) {
            outputHtml = outputHtml.replace(regexBbCodes[i], bbCodeHtml[i]);
        }

        return outputHtml;
    };

    VbpcComment.prototype.toHtmlRow = function () {
        var time = Util.getTimeElement(this.timestamp);
        var canMention = this.user.userID != WCF.User.userID;
        var canDelete = this.user.userID == WCF.User.userID || Context.isAdmin;

        var commentHtml = '';
        commentHtml += '<tr id="vbpc-comment-' + this.commentID + '">';
        commentHtml += '<td><a href="' + this.getProfileUrl() + '">' + this.user.username + '</a></td>';
        commentHtml += '<td>' + this.toString() + '</td>';
        commentHtml += '<td>' + time + '</td>';

        commentHtml += '<td>';
        if (canMention) {
            var mentionUsers = new Array();

            for (var i = 0; i < this.mentionedUserInfo.length; i++) {
                mentionUsers[i] = this.mentionedUserInfo[i].username;
            }

            commentHtml += '<a href="javascript:void(0)" class="jsTooltip vbpc-mention" title="' + Lang.MentionButton + '"  \
							id="vbpc-mention' + this.commentID + '" data-mention-users="' + mentionUsers.join() + '" data-author="' + this.user.username + '">';
            commentHtml += '<span class="icon icon16 icon-quote-left"></span>';
            commentHtml += '</a> ';
        }
        commentHtml += '</td>';

        commentHtml += '<td>';
        if (canDelete) {
            commentHtml += '<a href="javascript:void(0)" class="jsTooltip vbpc-delete" title="' + Lang.DeleteButton + '" \
							id="vbpc-delete' + this.commentID + '" data-comment-id="' + this.commentID + '">';
            commentHtml += '<span class="icon icon16 icon-remove"></span>';
            commentHtml += '</a>';
        }
        commentHtml += '</td>';
        commentHtml += '</tr>';

        return commentHtml;
    };
    return VbpcComment;
})();
/// <reference path="../api/VbpcComment.ts"/>
var CommentContainer = (function () {
    function CommentContainer(postId, postFooter) {
        this.inputActive = false;
        this.comments = new Array();
        this.postId = postId;
        this.$postFooter = postFooter;
        this.comments = new Array();
    }
    /**
    * Shows the container, applies the comments and updates the count-badge.
    * @param VbpcComment[] comments
    */
    CommentContainer.prototype.setComments = function (comments) {
        // Force comments to be an array
        if (comments == null) {
            comments = new Array();
        }

        // Store the comments
        this.comments = comments;

        // Create container
        this.ensureContainer();

        // Set badge's content
        this.$badge.text(this.comments.length.toString());

        // Remove previous comments (last-child is the input row)
        this.$tbody.find('tr:not(:last-child)').remove();

        // No comments - no container
        if (comments.length == 0) {
            this.$container.hide();
        }

        // Sort comments by timestamp
        this.comments.sort(function (a, b) {
            return a.timestamp - b.timestamp;
        });

        for (var i = 0; i < this.comments.length; i++) {
            this.$commentTextBoxRow.before(this.comments[i].toHtmlRow());
        }

        // Fix time elements
        WCF.Date.Time.prototype._domNodeInserted();

        var _this = this;

        // Mention button
        this.$tbody.find('tr:not(:last-child) > td > .vbpc-mention').click(function () {
            // Fetch users to mention
            var users = null;
            var dataMentionUsers = $(this).data('mention-users').split(',');
            if (dataMentionUsers.length == 0 || dataMentionUsers[0] == '') {
                users = new Array();
            } else {
                users = dataMentionUsers;
            }
            users.push($(this).data('author'));

            // Were we mentioned?
            if (users.indexOf(WCF.User.username) > -1) {
                // We don't want to mention ourself
                users.splice(users.indexOf(WCF.User.username), 1);
            }

            // Why not?
            users.sort();

            // Call the mention handler
            _this.comment_mention.call(_this, users);
        });

        // Delete button
        this.$tbody.find('tr:not(:last-child) > td > .vbpc-delete').click(function () {
            // Fetch comment id
            var commentID = parseInt($(this).data('comment-id'));

            // Call the delete handler
            _this.comment_delete.call(_this, commentID);
        });
    };

    /**
    * Creates the comment container.
    */
    CommentContainer.prototype.ensureContainer = function () {
        if (this.$container == null) {
            var htmlTable = '<table class="table"><tbody id="vbpc-tbody' + this.postId + '">' + '<tr id="vbpc-comment-input-row' + this.postId + '">' + '<td>' + WCF.User.username + '</td><td><textarea id="vbpc-comment-input-textbox' + this.postId + '" maxlength="' + Config.CommentMaxLength + '" rows="5" placeholder="' + Lang.CommentPlaceholder + '"></textarea></td><td></td><td></td>' + '</tr>' + '</tbody></table>';

            var htmlHeader = '<header><h3>' + Lang.Comments + ' <span class="badge" id="vbpc-comment-badge' + this.postId + '">0</span></h3></header>';
            var htmlBody = '<div>' + htmlTable + '</div>';
            var html = '<div class="vbpcComments"><div class="container containerPadding marginTop" id="vbpc-comment-container' + this.postId + '">' + htmlHeader + htmlBody + '</div></div>';

            // Create container with basic contents (heading, input box, ...)
            this.$container = $(html);
            this.$postFooter.append(this.$container);

            // Find the container components
            this.$badge = $('#vbpc-comment-badge' + this.postId);
            this.$tbody = $('#vbpc-tbody' + this.postId);
            this.$commentTextBox = $('#vbpc-comment-input-textbox' + this.postId);
            this.$commentTextBoxRow = $('#vbpc-comment-input-row' + this.postId);

            // Disable the input box
            this.inputActive = true;
            this.disableTextBox(true);

            // Apply event handlers to the input box
            var _this = this;
            this.$commentTextBox.keydown(function (e) {
                $.proxy(_this.input_KeyDown, _this)(e);
            });

            this.$commentTextBox.focusout(function (e) {
                $.proxy(_this.input_KeyDown, _this)(e);
            });
        } else {
            this.$container.show();
        }
    };

    /**
    * Called, when the user presses a key.
    * @param any e Eventdata
    */
    CommentContainer.prototype.input_KeyDown = function (e) {
        switch (e.keyCode) {
            case 13:
                // Shift + Return for line break
                if (e.shiftKey) {
                    return;
                }

                // Fetch text
                var text = this.$commentTextBox.val().trim();

                // If the text does not consist of whitespaces, send that comment
                if (text != '') {
                    // Pass comment to API and refresh comments
                    API.postComment(this.postId, text, function () {
                        Client.refreshComments();
                    });
                }

            case 27:
                this.disableTextBox();
                break;
        }
    };

    /**
    * Called, when the textbox loses the focus.
    * @param any e Eventdata
    */
    CommentContainer.prototype.input_FocusOut = function (e) {
        // Fetch text
        var text = this.$commentTextBox.val().trim();

        // Reset the input only, if the textbox is empty.
        if (text == '') {
            // Disable the textbox. Seriously, isn't that obvious?
            this.disableTextBox();
        }
    };

    /**
    * Called, when the mention button is clicked
    * @param string[] users
    */
    CommentContainer.prototype.comment_mention = function (users) {
        this.enableTextBox();

        for (var i = 0; i < users.length; i++) {
            users[i] = '@' + users[i];
        }

        this.$commentTextBox.val(users.join(' ') + ' ');
    };

    /**
    * Called, when the delete button is clicked
    * @param number commentID
    */
    CommentContainer.prototype.comment_delete = function (commentID) {
        API.deleteComment(commentID, function () {
            Client.refreshComments();
        });
    };

    /**
    * Resets and hides the textbox.
    */
    CommentContainer.prototype.disableTextBox = function (dontHideContainer) {
        if (typeof dontHideContainer === "undefined") { dontHideContainer = false; }
        if (this.inputActive) {
            // Ensure that the container is present
            this.ensureContainer();

            // Reset the textbox, hide it and set the editing status
            this.$commentTextBox.val('');
            this.$commentTextBoxRow.hide();
            this.inputActive = false;

            // If there are no other comments, hide the box also.
            if (this.comments.length == 0 && !dontHideContainer) {
                this.$container.hide();
            }
        }
    };

    /**
    * Shows the textbox.
    */
    CommentContainer.prototype.enableTextBox = function () {
        if (this.inputActive == false) {
            // Ensure that the container is present
            this.ensureContainer();

            // Show the textbox and set the editing status
            this.$commentTextBoxRow.show();
            this.$commentTextBox.focus();
            this.inputActive = true;
        }
    };
    return CommentContainer;
})();
/// <reference path="lib/jquery.d.ts"/>
/// <reference path="api/VbpcComment.ts"/>
/// <reference path="ui/CommentContainer.ts"/>
/// <reference path="Config.ts"/>
var Post = (function () {
    function Post(postId) {
        this.inputActive = false;
        this.commentCount = 0;
        this.postId = postId;

        // Search post and sub-elements
        this.$post = $('li#post' + this.postId).children('article');
        this.$postOptions = this.$post.children('div').children('section.messageContent').children('div').children('div.messageBody').children('footer.messageOptions').children('nav').children('ul');
        this.$postFooter = this.$post.children('div').children('section.messageContent').children('div').children('div.messageBody').children('div.messageFooter');

        // Initialize post
        this.applyPostOptions();
        this.container = new CommentContainer(this.postId, this.$postFooter);
        //this.ApplyComments();
    }
    Post.prototype.applyComments = function (comments) {
        this.container.setComments(comments);
    };

    Post.prototype.applyPostOptions = function () {
        var button = $('<li class="commentButton"><a href="javascript:void(0)" class="button jsTooltip" title="' + Lang.CommentButton + '" data-post-id=' + this.postId + '">' + '<span class="icon icon16 icon-comment"></span>' + '<span class="invisible">' + Lang.CommentButton + '</span>' + '</a></li>');
        this.$postOptions.prepend(button);

        var editButton = this.$postOptions.find('.jsMessageEditButton');
        if (editButton.length == 0) {
            this.$postOptions.prepend(button);
        } else {
            editButton.parent().after(button);
        }

        var _this = this;
        button.click(function () {
            _this.container.enableTextBox();
        });
    };
    return Post;
})();
var Lang = (function () {
    function Lang() {
    }
    Lang.VbpTitle = 'VB-Paradise 2.0 — Die große Visual–Basic– und .NET–Community';
    Lang.VbpcTabTitle = 'VBPC';
    Lang.Mentions = 'Erwähnungen';
    Lang.Comments = 'Kommentare';

    Lang.CommentButton = 'Kommentieren';
    Lang.MentionButton = 'Erwähnen';
    Lang.DeleteButton = 'Löschen';

    Lang.ErrorTitle = 'Fehler';
    Lang.DeleteConfirmation = 'Kommentar wirklich löschen?';
    Lang.DeletionFailed = 'Der Kommentar konnte nicht gelöscht werden.';

    Lang.RegistrationTitle = 'VBPC-Installation';
    Lang.RegistrationSubTitle = 'VBPC registriert nun deinen Account. Bitte verlasse diese Seite nicht und benutze VB-Paradise nicht, bis die Registrierung abgeschlossen ist.';

    Lang.CommentPlaceholder = '[Enter] zum Absenden; [Shift] + [Enter] für Zeilenumbruch; [Esc] zum Abbrechen.';

    Lang.Weekdays = new Array('Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag');
    Lang.Months = new Array('Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember');
    return Lang;
})();
/// <reference path="../lib/jquery.d.ts"/>
/// <reference path="../Lang.ts"/>
var RegistrationDialog = (function () {
    function RegistrationDialog() {
        var subtitle = Lang.RegistrationSubTitle;
        var dlg = $('<div id="vbpc-registration-dialog"><h2>' + subtitle + '</h2><ol id="vbpc-status-list" class="marginTop"></ol></div>');
        dlg.wcfDialog({
            title: Lang.RegistrationTitle,
            closable: false,
            modal: true
        });
    }
    RegistrationDialog.prototype.close = function () {
        var dlg = $('#vbpc-registration-dialog');

        //dlg.wcfDialog('close');
        dlg.wcfDialog({ closable: true });
    };

    RegistrationDialog.prototype.appendStatus = function (message) {
        $('#vbpc-status-list').append('<li>' + message + '</li>');
    };
    return RegistrationDialog;
})();
/// <reference path="ui/RegistrationDialog.ts"/>
/// <reference path="lib/WCF.User.d.ts"/>
var Registration = (function () {
    function Registration() {
    }
    Registration.prototype.checkAndDo = function (callback) {
        var _this = this;
        this.callback = callback;

        if (localStorage.getItem('vbpc_token') == null) {
            if (WCF.User.userID > 0) {
                this.dialog = new RegistrationDialog();

                this.dialog.appendStatus('Prüfe Account...');
                API.checkAccount(function (data) {
                    switch (data.result) {
                        case 'USER_NOT_INVITED':
                            _this.dialog.appendStatus('Der Server hat den Zugriff verweigert. Diese Erweiterung ist nur für ausgewählte Benutzer verfügbar.');
                            return;

                        case 'USER_IS_REGISTERED':
                            _this.install();
                            return;

                        case 'USER_NOT_REGISTERED':
                            _this.register();
                            return;
                    }
                });
            }
        } else {
            this.callback();
        }
    };

    Registration.prototype.saveToken = function (token) {
        this.dialog.appendStatus('Speichere Token...');
        localStorage.setItem('vbpc_token', token);
        this.dialog.appendStatus('Installation abgeschlossen. Der Dialog kann nun geschlossen werden.');
        this.dialog.close();
        this.callback();
    };

    Registration.prototype.register = function () {
        var _this = this;
        this.dialog.appendStatus('Registriere Account...');
        API.register(function (data) {
            _this.dialog.appendStatus('Dein Token lautet: <code class="inlineCode">' + data.token + '</code>');
            _this.dialog.appendStatus('<span style="color:red;font-weight:bold;">Bewahre diesen Token sicher auf! Er ist deine eindeutige ID und wird für jede Neuinstallation benötigt!</span>');
            _this.saveToken(data.token);
        });
    };

    Registration.prototype.install = function () {
        var _this = this;
        this.dialog.appendStatus('Account bereits registriert; Token benötigt:');
        this.dialog.appendStatus('<form><input type="text" class="medium" id="vbpc-token-input" /> <a href="javascript:void(0)" id="vbpc-token-submit" class="button"><span class="icon icon16 icon-ok"></span> <span>Fortfahren</span></a></form>');
        $('#vbpc-token-submit').click(function () {
            var token = $('#vbpc-token-input').val();
            $('#vbpc-token-input').parent().parent().remove();

            _this.dialog.appendStatus('Prüfe Token...');
            API.checkToken(token, function (res) {
                if (res) {
                    _this.saveToken(token);
                } else {
                    _this.dialog.appendStatus('<span style="color:red;font-weight:bold;">Dieser Token ist ungültig. Bitte lade die Seite neu, um es erneut zu versuchen.</span>');
                }
            });
        });
        $('#vbpc-token-input').focus();
    };
    return Registration;
})();
/// <reference path="Lang.ts"/>
var Util = (function () {
    function Util() {
    }
    Util.EndsWith = function (str, suffix) {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    };

    Util.ParseQuery = function (query) {
        // Initialize array
        var parsedArgs = new Array();

        // Split assignments
        if (query.indexOf('?') !== -1) {
            query = query.split('?')[1];
        }
        var assignments = query.split('&');

        // Any assignment supplied?
        if (assignments.length == 0 || assignments[0] == '') {
            // Nope, return
            return new Array();
        } else {
            for (var i = 0; i < assignments.length; i++) {
                var arg = assignments[i].split('=');
                var key = decodeURIComponent(arg[0]);
                var val = decodeURIComponent(arg[1]);
                if (typeof val == 'undefined')
                    val = null;
                parsedArgs[key] = val;
            }

            // Return result
            return parsedArgs;
        }
    };

    Util.getDate = function (date) {
        return date.getDate() + '. ' + Lang.Months[date.getMonth()] + ' ' + date.getFullYear();
    };

    Util.getTime = function (date) {
        return date.getHours() + ':' + date.getMinutes();
    };

    Util.getTimeElement = function (timestamp) {
        return '<time class="datetime" data-timestamp="' + timestamp + '" data-date="' + Util.getDate(new Date(timestamp * 1000)) + '" data-time="' + Util.getTime(new Date(timestamp * 1000)) + '">' + '</time>';
    };
    return Util;
})();
/// <reference path="../../Config.ts"/>
var VbpcPage = (function () {
    function VbpcPage(headline, breadcrumbs) {
        if (typeof breadcrumbs === "undefined") { breadcrumbs = new Array(); }
        var breadcrumbsHtml = '<nav class="breadcrumbs marginTop"><ul><li title="VB-Paradise 2.0 — Die große Visual–Basic– und .NET–Community" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">' + '<a href="https://www.vb-paradise.de/" itemprop="url"><span itemprop="title">VB-Paradise 2.0 — Die große Visual–Basic– und .NET–Community</span></a> <span class="pointer"><span>»</span></span></li>';
        for (var i = 0; i < breadcrumbs.length; i += 2) {
            var bcTitle = breadcrumbs[i];
            var bcLink = breadcrumbs[i + 1];
            breadcrumbsHtml += '<li title="' + bcTitle + '" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="' + bcLink + '" itemprop="url"><span itemprop="title">' + bcTitle + '</span></a> <span class="pointer"><span>»</span></span></li>';
        }
        breadcrumbsHtml += '</ul></nav>';

        var header = '<header class="boxHeadline"><h1>' + headline + '</header>';
        $('div#main').html('<div><div><section id="content" class="content">' + breadcrumbsHtml + header + '<div class="marginTop"></div>' + breadcrumbsHtml + '</section></div></div>');

        $('nav.navigation.navigationHeader > ul.navigationMenuItems').html('<li><a href="' + Config.VbpcTabMentions + '">Erwähnungen</a></li>' + '<li><a href="' + Config.VbpcTabComments + '">Kommentare</a></li>');

        this.$content = $('section#content > div.marginTop');
    }
    return VbpcPage;
})();
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
/// <reference path="VbpcPage.ts"/>
/// <reference path="../../Config.ts"/>
var MentionsPage = (function (_super) {
    __extends(MentionsPage, _super);
    function MentionsPage() {
        var _this = this;
        _super.call(this, 'Erwähnungen', [
            'VBPC', Config.VbpcTabUrl,
            'Erwähnungen', Config.VbpcTabMentions]);

        this.$content.html('<p class="info">Es wurden keine Einträge gefunden!</p>');

        API.getMentions(function (comments) {
            if (comments != null && comments.length > 0) {
                var table = '<div class="tabularBox tabularBoxTitle messageGroupList wbbThreadList"><header><h2>Erwähnungen</h2></header><table class="table">' + '<thead><tr>' + '<th class="columnTitle columnSubject">Post</th>' + '<th class="columnText columnReplies">Autor</th>' + '<th class="columnText">Kommentar</th>' + '</tr></thead><tbody id="vbpc-mentions-list"></tbody></table></div>';
                _this.$content.html(table);

                for (var j = 0; j < comments.length; j++) {
                    var c = new VbpcComment(comments[j]);
                    $('#vbpc-mentions-list').append('<tr>' + '<td><a href="' + Config.TargetBaseUrl + 'Thread/?postID=' + c.postID + '#post' + c.postID + '">' + c.postTitle + '</a></td>' + '<td><a href="' + c.user.getProfileUrl() + '">' + c.user.username + '</a></td>' + '<td>' + c.toString() + '</td>' + '</tr>');
                }
            }
        });
    }
    return MentionsPage;
})(VbpcPage);
/// <reference path="VbpcPage.ts"/>
/// <reference path="../../Config.ts"/>
/// <reference path="../RegistrationDialog.ts"/>
var CommentsPage = (function (_super) {
    __extends(CommentsPage, _super);
    function CommentsPage() {
        var _this = this;
        _super.call(this, 'Letze Kommentare', [
            'VBPC', Config.VbpcTabUrl,
            'Letze Kommentare', Config.VbpcTabComments]);

        this.$content.html('<p class="info">Es wurden keine Einträge gefunden!</p>');

        API.getComments(null, function (comments) {
            if (comments != null && comments.length > 0) {
                var table = '<div class="tabularBox tabularBoxTitle messageGroupList wbbThreadList"><header><h2>Kommentare</h2></header><table class="table">' + '<thead><tr>' + '<th class="columnTitle columnSubject">Post</th>' + '<th class="columnText columnReplies">Autor</th>' + '<th class="columnText">Kommentar</th>' + '</tr></thead><tbody id="vbpc-mentions-list"></tbody></table></div>';
                _this.$content.html(table);

                for (var j = 0; j < comments.length; j++) {
                    var c = new VbpcComment(comments[j]);
                    $('#vbpc-mentions-list').append('<tr>' + '<td><a href="' + Config.TargetBaseUrl + 'Thread/?postID=' + c.postID + '#post' + c.postID + '">' + c.postTitle + '</a></td>' + '<td><a href="' + c.user.getProfileUrl() + '">' + c.user.username + '</a></td>' + '<td>' + c.toString() + '</td>' + '</tr>');
                }
            }
        });
    }
    return CommentsPage;
})(VbpcPage);
/// <reference path="../lib/jquery.d.ts"/>
/// <reference path="../Config.ts"/>
/// <reference path="../Lang.ts"/>
/// <reference path="../Context.ts"/>
/// <reference path="../Util.ts"/>
/// <reference path="pages/MentionsPage.ts"/>
/// <reference path="pages/CommentsPage.ts"/>
var VbpcTab = (function () {
    function VbpcTab() {
        var query = Util.ParseQuery(document.location.search);
        this.active = (typeof query['vbpc'] != 'undefined');

        // Top navigation
        var tabHtml = '<li><a href="' + Config.VbpcTabMentions + '"><span class="icon icon16 icon-comment"></span> <span>' + Lang.Mentions + '</span>';
        if (Context.unreadMentions > 0)
            tabHtml += ' <span class="badge badgeUpdate">' + Context.unreadMentions.toString() + '</span>';
        tabHtml += '</a></li>';
        $("#topMenu > div > ul").append(tabHtml);

        // Main navigation
        tabHtml = '<li';
        if (this.active)
            tabHtml += ' class="active"';
        tabHtml += '><a href="' + Config.VbpcTabUrl + '">' + Lang.VbpcTabTitle + '</a></li>';

        if (this.active)
            $('#mainMenu > ul > li.active').removeClass('active');
        $('#mainMenu > ul').append(tabHtml);

        if (this.active) {
            // Page title
            document.title = Lang.VbpcTabTitle + " - " + Lang.VbpTitle;

            switch (query['vbpc']) {
                case 'mentions':
                    new MentionsPage();
                    break;

                case 'comments':
                    new CommentsPage();
                    break;

                default:
                    new MentionsPage();
                    break;
            }
        }
    }
    return VbpcTab;
})();
/// <reference path="api/API.ts"/>
/// <reference path="Config.ts"/>
/// <reference path="Context.ts"/>
/// <reference path="lib/WCF.User.d.ts"/>
/// <reference path="Post.ts"/>
/// <reference path="Registration.ts"/>
/// <reference path="ui/VbpcTab.ts"/>
var Client = (function () {
    function Client() {
        var _this = this;
        // Initialize utils
        new Util();

        // Check, if user is logged in
        if (WCF.User.username != '' && WCF.User.userID > 0 && document.body.id != 'tplRedirect') {
            // Do registration
            var reg = new Registration();
            reg.checkAndDo(function () {
                // Apply stylesheet
                var link = document.createElement('link');
                link.href = Config.StylesheetUrl;
                link.rel = 'stylesheet';
                link.type = 'text/css';
                document.head.appendChild(link);

                // Fetch thread title
                Context.threadTitle = document.title.split('-')[0]; // Output: (3) [C++]Auf privaten statischen Member zugreifen ?
                Context.threadTitle = Context.threadTitle.replace(/\([0-9]+\)/, '').trim(); // Output: [C++]Auf privaten statischen Member zugreifen ?

                // Create arrays
                Client.posts = new Array();
                Client.postIds = new Array();

                // Load context
                API.getContext(function (data) {
                    Context.isAdmin = data.isAdmin;
                    Context.unreadMentions = data.unreadMentions;

                    // Apply tabs and pages
                    new VbpcTab();

                    if (document.body.id == 'tplThread') {
                        _this.preparePosts();
                        Client.refreshComments();
                    }
                });
            });
        }
    }
    Client.prototype.preparePosts = function () {
        // Apply post options and comments
        // Fetch posts
        var postData = $('ul.wbbThreadPostList').children('li').children('article');

        for (var i = 0; i < postData.length; i++) {
            var currentPostId = postData[i].getAttribute('data-object-id');

            if (currentPostId != null) {
                // This post is valid
                var postid = parseInt(currentPostId);
                Client.posts.push(new Post(postid));
                Client.postIds.push(postid);
            }
        }
    };

    Client.refreshComments = function () {
        // Fetch comments and apply them
        API.getComments(Client.postIds, function (comments) {
            for (var i = 0; i < Client.postIds.length; i++) {
                var id = Client.postIds[i].toString();
                if (comments.hasOwnProperty(id)) {
                    var c = new Array();
                    var src = comments[id];

                    for (var j = 0; j < src.length; j++) {
                        c.push(new VbpcComment(src[j]));
                    }

                    Client.posts[i].applyComments(c);
                } else {
                    Client.posts[i].applyComments(null);
                }
            }
        });
    };
    return Client;
})();
/// <reference path="Client.ts"/>
new Client();
