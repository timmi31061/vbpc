/// <reference path="Lang.ts"/>
class Util {

	constructor() {
	}

	public static EndsWith(str: string, suffix: string) {
		return str.indexOf(suffix, str.length - suffix.length) !== -1;
	}

	public static ParseQuery(query: string) {
		// Initialize array
		var parsedArgs = new Array();

		// Split assignments
		if(query.indexOf('?') !== -1) {
			query = query.split('?')[1]
		}
		var assignments = query.split('&');
		
		// Any assignment supplied?
		if(assignments.length == 0 || assignments[0] == '') {
			// Nope, return
			return new Array();
		} else {
			// Yep, parse assignments
			for(var i: number = 0; i < assignments.length; i++) {
				var arg = assignments[i].split('=');
				var key = decodeURIComponent(arg[0]);
				var val = decodeURIComponent(arg[1]);
				if(typeof val == 'undefined') val = null;
				parsedArgs[key] = val;
			}

			// Return result
			return parsedArgs;
		}
	}

	public static getDate(date: Date) {
		return date.getDate() + '. ' + Lang.Months[date.getMonth()] + ' ' + date.getFullYear();
	}

	public static getTime(date: Date) {
		return date.getHours() + ':' + date.getMinutes();
	}

	public static getTimeElement(timestamp) {
		return '<time class="datetime" data-timestamp="' + timestamp + '" data-date="' + Util.getDate(new Date(timestamp * 1000)) + '" data-time="' + Util.getTime(new Date(timestamp * 1000)) + '">' + '</time>';
	}
}
