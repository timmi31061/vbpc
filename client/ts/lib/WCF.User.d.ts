
interface WcfUser {
	username: string;
	userID: number;
}

interface WcfDate {
	Time: WcfDateTime;
}

interface WcfDateTime {
	prototype: any
}

declare class WCF {
	static User: WcfUser;
	static Date: WcfDate;
}
