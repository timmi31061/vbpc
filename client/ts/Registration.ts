/// <reference path="ui/RegistrationDialog.ts"/>
/// <reference path="lib/WCF.User.d.ts"/>
class Registration {
	private callback: () => void;
	private dialog: RegistrationDialog;

	public checkAndDo(callback: () => void) {
		this.callback = callback;

		if(localStorage.getItem('vbpc_token') == null) {
			if(WCF.User.userID > 0) {
				this.dialog = new RegistrationDialog();
				
				this.dialog.appendStatus('Prüfe Account...');
				API.checkAccount((data: any) => {
					switch(data.result) {
						case 'USER_NOT_INVITED':
							this.dialog.appendStatus('Der Server hat den Zugriff verweigert. Diese Erweiterung ist nur für ausgewählte Benutzer verfügbar.');
							return;

						case 'USER_IS_REGISTERED':
							this.install();
							return;

						case 'USER_NOT_REGISTERED':
							this.register();
							return;
					}
				});
			}
		} else {
			this.callback();
		}
	}

	private saveToken(token: string) {
		this.dialog.appendStatus('Speichere Token...');
		localStorage.setItem('vbpc_token', token);
		this.dialog.appendStatus('Installation abgeschlossen. Der Dialog kann nun geschlossen werden.');
		this.dialog.close();
		this.callback();
	}

	private register() {
		this.dialog.appendStatus('Registriere Account...');
		API.register((data: any) => {
			this.dialog.appendStatus('Dein Token lautet: <code class="inlineCode">' + data.token + '</code>');
			this.dialog.appendStatus('<span style="color:red;font-weight:bold;">Bewahre diesen Token sicher auf! Er ist deine eindeutige ID und wird für jede Neuinstallation benötigt!</span>');
			this.saveToken(data.token);
		});
	}

	private install() {
		this.dialog.appendStatus('Account bereits registriert; Token benötigt:');
		this.dialog.appendStatus('<form><input type="text" class="medium" id="vbpc-token-input" /> <a href="javascript:void(0)" id="vbpc-token-submit" class="button"><span class="icon icon16 icon-ok"></span> <span>Fortfahren</span></a></form>');
		$('#vbpc-token-submit').click(() => {
			var token = $('#vbpc-token-input').val();
			$('#vbpc-token-input').parent().parent().remove();

			this.dialog.appendStatus('Prüfe Token...');
			API.checkToken(token, (res: boolean) => {
				if(res) {
					this.saveToken(token);
				} else {
					this.dialog.appendStatus('<span style="color:red;font-weight:bold;">Dieser Token ist ungültig. Bitte lade die Seite neu, um es erneut zu versuchen.</span>');
				}
			});
		});
		$('#vbpc-token-input').focus();
	}
}