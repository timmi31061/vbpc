class Config {
	static CurrentVersion: string = '3.0.0';

	static ProtocolPrefix: string = document.location.protocol + '//';
	//static ProtocolPrefix: string = 'http://';
	static IsHttps: boolean = document.location.protocol == 'https:';

	static TargetHostName: string = 'www.vb-paradise.de';
	static TargetBaseUrl: string = Config.ProtocolPrefix + Config.TargetHostName + '/index.php/';
	static ProfileUrlTemplate: string = Config.TargetBaseUrl + 'User/';

	static ServiceHostName: string = 'xmpp.tforge.de';
	static ServiceBaseUrl: string = Config.ProtocolPrefix + Config.ServiceHostName + '/';
	static ApiBaseUrl: string = Config.ServiceBaseUrl + 'api/';
	static ClientUrl: string = Config.ServiceBaseUrl + 'client/';
	static StylesheetUrl: string = Config.ClientUrl + 'css/style.css';

	static VbpcTabUrl: string = '/index.php/MembersList/?vbpc=vbpc'
	static VbpcTabComments: string = '/index.php/MembersList/?vbpc=comments';
	static VbpcTabMentions: string = '/index.php/MembersList/?vbpc=mentions';

	static CommentMaxLength: number = 32768;
}
