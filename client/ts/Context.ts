class Context {
	public static unreadMentions: number = 0;
	public static isAdmin: boolean = false;
	public static threadTitle: string = "";
}