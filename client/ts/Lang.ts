class Lang {
	// Page and tab titles
	static VbpTitle: string = 'VB-Paradise 2.0 — Die große Visual–Basic– und .NET–Community';
	static VbpcTabTitle: string = 'VBPC'
	static Mentions: string = 'Erwähnungen';
	static Comments: string = 'Kommentare';

	// Buttons
	static CommentButton: string = 'Kommentieren';
	static MentionButton: string = 'Erwähnen';
	static DeleteButton: string = 'Löschen';

	// Messages
	static ErrorTitle: string = 'Fehler';
	static DeleteConfirmation: string = 'Kommentar wirklich löschen?';
	static DeletionFailed: string = 'Der Kommentar konnte nicht gelöscht werden.';

	// Registration dialog
	static RegistrationTitle: string = 'VBPC-Installation';
	static RegistrationSubTitle: string = 'VBPC registriert nun deinen Account. Bitte verlasse diese Seite nicht und benutze VB-Paradise nicht, bis die Registrierung abgeschlossen ist.';

	static CommentPlaceholder: string = '[Enter] zum Absenden; [Shift] + [Enter] für Zeilenumbruch; [Esc] zum Abbrechen.';

	static Weekdays = new Array('Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag');
	static Months = new Array('Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember');
}
