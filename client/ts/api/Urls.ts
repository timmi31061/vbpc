/// <reference path="../Config.ts"/>
class Urls {
	static Comments = Config.ApiBaseUrl + 'index.php/Comments';
	static Context = Config.ApiBaseUrl + 'index.php/Context';
	static Post = Config.ApiBaseUrl + 'index.php/Post';
	static Delete = Config.ApiBaseUrl + 'index.php/Delete';
	static Install = Config.ApiBaseUrl + 'index.php/Install';
}