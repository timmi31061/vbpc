/// <reference path="Urls.ts"/>
/// <reference path="../Client.ts"/>
/// <reference path="../Context.ts"/>
/// <reference path="../lib/WCF.User.d.ts"/>
class API {
	private static request(url, data, callback: (data: any) => void) {
		if(data == null) {
			data = new Object();
		}
		data['token'] = localStorage.getItem('vbpc_token');

		$.post(url, data, callback, 'json');
	}

	public static postComment(postID: number, message: string, callback: () => void) {
		var data = new Object();
		data['postID'] = postID;
		data['postTitle'] = Context.threadTitle;
		data['message'] = message;

		API.request(Urls.Post, data, callback);
	}

	public static deleteComment(commentID: number, callback: () => void) {
		var data = new Object();
		data['commentID'] = commentID;

		API.request(Urls.Delete, data, callback);
	}

	public static getComments(postIDs: number[], callback: (comments: any) => void) {
		var data = new Object();
		if(postIDs != null) {
			data['postIDs'] = postIDs.join(',');
		}

		API.request(Urls.Comments, data, callback);
	}

	public static getMentions(callback: (comments: any) => void) {
		var data = new Object();
		data['readMentions'] = true;

		API.request(Urls.Comments, data, callback);
	}

	public static getContext(callback: (data: any) => void) {
		var data = new Object();
		API.request(Urls.Context, data, callback);
	}

	public static checkAccount(callback: (data: any) => void) {
		var data = new Object();
		data['action'] = 'checkRegister';
		data['username'] = WCF.User.username;

		API.request(Urls.Install, data, callback);
	}

	public static register(callback: (data: any) => void) {
		var data = new Object();
		data['action'] = 'register';
		data['userID'] = WCF.User.userID;
		data['username'] = WCF.User.username;

		API.request(Urls.Install, data, callback);
	}

	public static checkToken(token: string, callback: (result: boolean) => void) {
		var data = new Object();
		data['token'] = token;

		$.post(Urls.Context, data, (resp: string) => {
			try {
				JSON.parse(resp);
				callback(true);
			} catch(e) {
				callback(false);
			}
		}, 'text');
	}
}