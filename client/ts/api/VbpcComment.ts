/// <reference path="../Config.ts"/>
/// <reference path="../Context.ts"/>
/// <reference path="User.ts"/>
class VbpcComment {
	public commentID: number;
	public timestamp: number;
	public postTitle: string;
	public postID: number;
	public user: User;
	public message: string;
	public mentionedUserInfo: User[];

	constructor(obj: any = null) {
		if(obj == null) {
			return;
		}

		for (var prop in obj) {
			this[prop] = obj[prop];
		}

		// Create own objects from the flat JSON-objects
		this.user = new User(this.user);

		for(var i = 0; i < this.mentionedUserInfo.length; i++) {
			this.mentionedUserInfo[i] = new User(this.mentionedUserInfo[i]);
		}
	}

	public getProfileUrl() {
		return this.user.getProfileUrl();
	}

	public toString() {
		var regexLink = /(https?:\/\/[^ ]+)/g;
		var regexBbCodes = new Array(/\[b\](.*?)\[\/b\]/gi, /\[i\](.*?)\[\/i\]/gi, /\[u\](.*?)\[\/u\]/gi, /\[s\](.*?)\[\/s\]/gi);
		var bbCodeHtml = new Array("<strong>$1</strong>", "<em>$1</em>", "<span style=\"text-decoration: underline\">$1</span>", "<span style=\"text-decoration: line-through\">$1</span>");

		var outputHtml = this.message;

		// Parse Links
		outputHtml = outputHtml.replace(regexLink, "<a href=\"$1\" title=\"\">$1</a>");

		// Parse mentions
		// Are mention user infos defined?
		if(this.mentionedUserInfo != null || this.mentionedUserInfo.length > 0) {
			for(var i = 0; i < this.mentionedUserInfo.length; i++) {
				var current = this.mentionedUserInfo[i];
				outputHtml = outputHtml.replace('@' + current.username, '<a href="' + current.getProfileUrl() + '">@' + current.username + '</a>');
			}
		}

		// Parse BB-Code
		for(var i = 0; i < regexBbCodes.length; i++) {
			outputHtml = outputHtml.replace(regexBbCodes[i], bbCodeHtml[i]);
		}

		return outputHtml;
	}

	public toHtmlRow() {
		var time = Util.getTimeElement(this.timestamp);
		var canMention = this.user.userID != WCF.User.userID;
		var canDelete = this.user.userID == WCF.User.userID || Context.isAdmin;

		var commentHtml = '';
		commentHtml += '<tr id="vbpc-comment-' + this.commentID + '">';
			commentHtml += '<td><a href="' + this.getProfileUrl() + '">' + this.user.username + '</a></td>';
			commentHtml += '<td>' + this.toString() + '</td>';
			commentHtml += '<td>' + time + '</td>';

				commentHtml += '<td>';
					if(canMention) {
						var mentionUsers: string[] = new Array();

						for(var i = 0; i < this.mentionedUserInfo.length; i++) {
							mentionUsers[i] = this.mentionedUserInfo[i].username
						}

							commentHtml += '<a href="javascript:void(0)" class="jsTooltip vbpc-mention" title="' + Lang.MentionButton + '"  \
							id="vbpc-mention' + this.commentID + '" data-mention-users="' + mentionUsers.join() + '" data-author="' + this.user.username + '">';
								commentHtml += '<span class="icon icon16 icon-quote-left"></span>';
							commentHtml += '</a> ';
					}
				commentHtml += '</td>'

				commentHtml += '<td>';
					if(canDelete) {
							commentHtml += '<a href="javascript:void(0)" class="jsTooltip vbpc-delete" title="' + Lang.DeleteButton + '" \
							id="vbpc-delete' + this.commentID + '" data-comment-id="' + this.commentID + '">';
								commentHtml += '<span class="icon icon16 icon-remove"></span>';
							commentHtml += '</a>';
					}
				commentHtml += '</td>'
		commentHtml += '</tr>';

		return commentHtml;
	}
}
