class BaseObject {
	constructor() {
	}
	
	constructor(obj: any) {
		for (var prop in obj) {
			this[prop] = obj[prop];
		}
	}
}