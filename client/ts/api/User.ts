/// <reference path="../Config.ts"/>
class User {
	public userID: number;
	public username: string;

	constructor(obj: any = null) {
		if(obj == null) {
			return;
		}
		
		for (var prop in obj) {
			this[prop] = obj[prop];
		}
	}

	public getProfileUrl() {
		return Config.ProfileUrlTemplate + this.userID.toString() + "-" + this.username;
	}

	public toString() {
		return this.username;
	}
}
