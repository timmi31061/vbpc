/// <reference path="../lib/jquery.d.ts"/>
/// <reference path="../Lang.ts"/>
class RegistrationDialog {
	constructor() {
		var subtitle = Lang.RegistrationSubTitle;
		var dlg: any = $('<div id="vbpc-registration-dialog"><h2>' + subtitle + '</h2><ol id="vbpc-status-list" class="marginTop"></ol></div>');
		dlg.wcfDialog({
			title: Lang.RegistrationTitle,
			closable: false,
			modal: true
		});
	}

	close() {
		var dlg: any = $('#vbpc-registration-dialog');
		//dlg.wcfDialog('close');
		dlg.wcfDialog({closable: true});
	}

	appendStatus(message: string) {
		$('#vbpc-status-list').append('<li>' + message + '</li>');
	}
}
