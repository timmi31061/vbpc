/// <reference path="../api/VbpcComment.ts"/>
class CommentContainer {
	postId: number;
	inputActive: boolean = false;
	comments: VbpcComment[] = new Array();

	$postFooter: JQuery;

	$container: JQuery;
	$badge: JQuery;
	$tbody: JQuery;
	$commentTextBox: JQuery;
	$commentTextBoxRow: JQuery;

	constructor(postId: number, postFooter: JQuery) {
		this.postId = postId;
		this.$postFooter = postFooter;
		this.comments = new Array();
	}

	/**
	 * Shows the container, applies the comments and updates the count-badge.
	 * @param VbpcComment[] comments
	 */
	public setComments(comments: VbpcComment[]) {
		// Force comments to be an array
		if(comments == null) {
			comments = new Array();
		}

		// Store the comments
		this.comments = comments;

		// Create container
		this.ensureContainer();

		// Set badge's content
		this.$badge.text(this.comments.length.toString());

		// Remove previous comments (last-child is the input row)
		this.$tbody.find('tr:not(:last-child)').remove();

		// No comments - no container
		if(comments.length == 0) {
			this.$container.hide();
		}

		// Sort comments by timestamp
		this.comments.sort(function(a, b) {
			return a.timestamp - b.timestamp;
		});

		// Apply the comments
		for(var i = 0; i < this.comments.length; i++) {
			this.$commentTextBoxRow.before(this.comments[i].toHtmlRow());
		}

		// Fix time elements
		WCF.Date.Time.prototype._domNodeInserted();

		var _this = this;

		// Mention button
		this.$tbody.find('tr:not(:last-child) > td > .vbpc-mention').click(function() {
			// Fetch users to mention
			var users: string[] = null;
			var dataMentionUsers = $(this).data('mention-users').split(',');
			if(dataMentionUsers.length == 0 || dataMentionUsers[0] == '') {
				users = new Array();
			} else {
				users = dataMentionUsers;
			}
			users.push($(this).data('author'));

			// Were we mentioned?
			if(users.indexOf(WCF.User.username) > -1) {
				// We don't want to mention ourself
				users.splice(users.indexOf(WCF.User.username), 1);
			}

			// Why not?
			users.sort();

			// Call the mention handler
			_this.comment_mention.call(_this, users);
		});

		// Delete button
		this.$tbody.find('tr:not(:last-child) > td > .vbpc-delete').click(function () {
			// Fetch comment id
			var commentID: number = parseInt($(this).data('comment-id'));
			
			// Call the delete handler
			_this.comment_delete.call(_this, commentID);
		});
	}

	/**
	 * Creates the comment container.
	 */
	public ensureContainer() {
		if(this.$container == null) {
			var htmlTable = '<table class="table"><tbody id="vbpc-tbody' + this.postId + '">' +
								'<tr id="vbpc-comment-input-row' + this.postId + '">' +
									'<td>' + WCF.User.username + '</td><td><textarea id="vbpc-comment-input-textbox' + this.postId + '" maxlength="' + Config.CommentMaxLength + '" rows="5" placeholder="' + Lang.CommentPlaceholder + '"></textarea></td><td></td><td></td>' +
								'</tr>' +
							'</tbody></table>';

			var htmlHeader = '<header><h3>' + Lang.Comments + ' <span class="badge" id="vbpc-comment-badge' + this.postId + '">0</span></h3></header>';
			var htmlBody = '<div>' + htmlTable + '</div>';
			var html = '<div class="vbpcComments"><div class="container containerPadding marginTop" id="vbpc-comment-container' + this.postId + '">' + htmlHeader + htmlBody + '</div></div>';

			// Create container with basic contents (heading, input box, ...)
			this.$container = $(html);
			this.$postFooter.append(this.$container);

			// Find the container components
			this.$badge = $('#vbpc-comment-badge' + this.postId);
			this.$tbody = $('#vbpc-tbody' + this.postId);
			this.$commentTextBox = $('#vbpc-comment-input-textbox' + this.postId);
			this.$commentTextBoxRow = $('#vbpc-comment-input-row' + this.postId);

			// Disable the input box
			this.inputActive = true;
			this.disableTextBox(true);

			// Apply event handlers to the input box
			var _this = this;
			this.$commentTextBox.keydown(function(e: any) {
				$.proxy(_this.input_KeyDown, _this)(e);
			});

			this.$commentTextBox.focusout(function(e: any) {
				$.proxy(_this.input_KeyDown, _this)(e);
			});
		} else {
			this.$container.show();
		}
	}

	/**
	 * Called, when the user presses a key.
	 * @param any e Eventdata
	 */
	public input_KeyDown(e: any) {
		switch(e.keyCode) {
			// Enter
			case 13:
				// Shift + Return for line break
				if(e.shiftKey) {
					return;
				}

				// Fetch text
				var text: string = this.$commentTextBox.val().trim();

				// If the text does not consist of whitespaces, send that comment
				if(text != '') {
					// Pass comment to API and refresh comments
					API.postComment(this.postId, text, () => {
						Client.refreshComments();
					});
				}

				// break is intentionally omitted
				
			// Escape
			case 27:
				this.disableTextBox();
				break;
		}
	}

	/**
	 * Called, when the textbox loses the focus.
	 * @param any e Eventdata
	 */
	public input_FocusOut(e: any) {
		// Fetch text
		var text: string = this.$commentTextBox.val().trim();

		// Reset the input only, if the textbox is empty.
		if(text == '') {
			// Disable the textbox. Seriously, isn't that obvious?
			this.disableTextBox();


		}
	}

	/**
	 * Called, when the mention button is clicked
	 * @param string[] users
	 */
	public comment_mention(users: string[]) {
		this.enableTextBox();

		for(var i = 0; i < users.length; i++) {
			users[i] = '@' + users[i];
		}

		this.$commentTextBox.val(users.join(' ') + ' ');
	}

	/**
	 * Called, when the delete button is clicked
	 * @param number commentID
	 */
	public comment_delete(commentID: number) {
		API.deleteComment(commentID, () => {
			Client.refreshComments();
		});
	}

	/**
	 * Resets and hides the textbox.
	 */
	public disableTextBox(dontHideContainer: boolean = false) {
		if(this.inputActive) {
			// Ensure that the container is present
			this.ensureContainer();

			// Reset the textbox, hide it and set the editing status
			this.$commentTextBox.val('');
			this.$commentTextBoxRow.hide();
			this.inputActive = false;

			// If there are no other comments, hide the box also.
			if(this.comments.length == 0 && !dontHideContainer) {
				this.$container.hide();
			}
		}
	}

	/**
	 * Shows the textbox.
	 */
	public enableTextBox() {
		if(this.inputActive == false) {
			// Ensure that the container is present
			this.ensureContainer();

			// Show the textbox and set the editing status
			this.$commentTextBoxRow.show();
			this.$commentTextBox.focus();
			this.inputActive = true;
		}
	}
}