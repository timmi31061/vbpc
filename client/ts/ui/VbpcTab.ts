/// <reference path="../lib/jquery.d.ts"/>
/// <reference path="../Config.ts"/>
/// <reference path="../Lang.ts"/>
/// <reference path="../Context.ts"/>
/// <reference path="../Util.ts"/>
/// <reference path="pages/MentionsPage.ts"/>
/// <reference path="pages/CommentsPage.ts"/>
class VbpcTab {
	active: boolean;
	constructor() {
		var query = Util.ParseQuery(document.location.search);
		this.active = (typeof query['vbpc'] != 'undefined');

		// Top navigation
		var tabHtml = '<li><a href="' + Config.VbpcTabMentions + '"><span class="icon icon16 icon-comment"></span> <span>' +  Lang.Mentions + '</span>';
		if(Context.unreadMentions > 0) tabHtml += ' <span class="badge badgeUpdate">' + Context.unreadMentions.toString() + '</span>';
		tabHtml += '</a></li>';
		$("#topMenu > div > ul").append(tabHtml);

		// Main navigation
		tabHtml = '<li';
		if(this.active) tabHtml += ' class="active"';
		tabHtml += '><a href="' + Config.VbpcTabUrl + '">' + Lang.VbpcTabTitle + '</a></li>';

		if(this.active) $('#mainMenu > ul > li.active').removeClass('active');
		$('#mainMenu > ul').append(tabHtml);

		if(this.active) {
			// Page title
			document.title = Lang.VbpcTabTitle + " - " + Lang.VbpTitle;

			// Load page
			switch(query['vbpc']) {
				case 'mentions':
					new MentionsPage();
					break;

				case 'comments':
					new CommentsPage();
					break;

				default:
					new MentionsPage();
					break;
			}
		}
	}
}
