/// <reference path="VbpcPage.ts"/>
/// <reference path="../../Config.ts"/>
/// <reference path="../RegistrationDialog.ts"/>
class CommentsPage extends VbpcPage {
	constructor () {
		super('Letze Kommentare', ['VBPC', Config.VbpcTabUrl,
							 'Letze Kommentare', Config.VbpcTabComments]);

		this.$content.html('<p class="info">Es wurden keine Einträge gefunden!</p>');

		API.getComments(null, (comments: any) => {
			if(comments != null && comments.length > 0) {
				var table: string = '<div class="tabularBox tabularBoxTitle messageGroupList wbbThreadList"><header><h2>Kommentare</h2></header><table class="table">' +
				'<thead><tr>' +
					'<th class="columnTitle columnSubject">Post</th>' +
					'<th class="columnText columnReplies">Autor</th>' +
					'<th class="columnText">Kommentar</th>' +
				'</tr></thead><tbody id="vbpc-mentions-list"></tbody></table></div>';
				this.$content.html(table);

				for(var j = 0; j < comments.length; j++) {
					var c = new VbpcComment(comments[j]);
					$('#vbpc-mentions-list').append('<tr>' +
						'<td><a href="' + Config.TargetBaseUrl + 'Thread/?postID=' + c.postID + '#post' + c.postID + '">' + c.postTitle + '</a></td>' +
						'<td><a href="' + c.user.getProfileUrl() + '">' + c.user.username + '</a></td>' +
						'<td>' + c.toString() + '</td>' +
					'</tr>');
				}
			}
		});
	}
}
