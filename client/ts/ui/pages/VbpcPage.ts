/// <reference path="../../Config.ts"/>
class VbpcPage {
	$content: JQuery;

	constructor(headline: string, breadcrumbs: string[] = new Array()) {
		var breadcrumbsHtml: string = '<nav class="breadcrumbs marginTop"><ul><li title="VB-Paradise 2.0 — Die große Visual–Basic– und .NET–Community" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">' +
			'<a href="https://www.vb-paradise.de/" itemprop="url"><span itemprop="title">VB-Paradise 2.0 — Die große Visual–Basic– und .NET–Community</span></a> <span class="pointer"><span>»</span></span></li>'
		for(var i = 0; i < breadcrumbs.length; i += 2) {
			var bcTitle = breadcrumbs[i];
			var bcLink = breadcrumbs[i + 1];
			breadcrumbsHtml += '<li title="' + bcTitle + '" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="' + bcLink + '" itemprop="url"><span itemprop="title">' + bcTitle + '</span></a> <span class="pointer"><span>»</span></span></li>'
		}
		breadcrumbsHtml += '</ul></nav>';

		var header: string = '<header class="boxHeadline"><h1>' + headline + '</header>';
		$('div#main').html(
			'<div><div><section id="content" class="content">' +  breadcrumbsHtml + header +
				'<div class="marginTop"></div>' + breadcrumbsHtml +
			'</section></div></div>');

		$('nav.navigation.navigationHeader > ul.navigationMenuItems').html('<li><a href="' + Config.VbpcTabMentions + '">Erwähnungen</a></li>' +
		                                                                   '<li><a href="' + Config.VbpcTabComments + '">Kommentare</a></li>');

		this.$content = $('section#content > div.marginTop');
	}
}