/// <reference path="lib/jquery.d.ts"/>
/// <reference path="api/VbpcComment.ts"/>
/// <reference path="ui/CommentContainer.ts"/>
/// <reference path="Config.ts"/>
class Post {
	postId: number;
	inputActive: boolean = false;
	commentCount: number = 0;

	$post: JQuery;
	$postOptions: JQuery;
	$postFooter: JQuery;

	container: CommentContainer;

	constructor(postId: number) {
		this.postId = postId;

		// Search post and sub-elements
		this.$post = $('li#post' + this.postId).children('article');
		this.$postOptions = this.$post.children('div').children('section.messageContent').children('div').children('div.messageBody').children('footer.messageOptions').children('nav').children('ul');
		this.$postFooter = this.$post.children('div').children('section.messageContent').children('div').children('div.messageBody').children('div.messageFooter');

		// Initialize post
		this.applyPostOptions();
		this.container = new CommentContainer(this.postId, this.$postFooter);
		//this.ApplyComments();
	}

	public applyComments(comments: VbpcComment[]) {
		this.container.setComments(comments);
	}

	private applyPostOptions() {
		var button = $('<li class="commentButton"><a href="javascript:void(0)" class="button jsTooltip" title="' + Lang.CommentButton + '" data-post-id=' + this.postId + '">' +
		                   '<span class="icon icon16 icon-comment"></span>' +
		                   '<span class="invisible">' + Lang.CommentButton + '</span>' +
		               '</a></li>');
		this.$postOptions.prepend(button);

		var editButton = this.$postOptions.find('.jsMessageEditButton');
		if(editButton.length == 0) {
			this.$postOptions.prepend(button);
		} else {
			editButton.parent().after(button);
		}

		var _this = this;
		button.click(function() {
			_this.container.enableTextBox();
		});
	}
}
