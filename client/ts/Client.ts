/// <reference path="api/API.ts"/>
/// <reference path="Config.ts"/>
/// <reference path="Context.ts"/>
/// <reference path="lib/WCF.User.d.ts"/>
/// <reference path="Post.ts"/>
/// <reference path="Registration.ts"/>
/// <reference path="ui/VbpcTab.ts"/>
class Client {
	public static posts: Post[];
	public static postIds: number[];

	constructor() {
		// Initialize utils
		new Util();

		// Check, if user is logged in
		if(WCF.User.username != '' && WCF.User.userID > 0 && document.body.id != 'tplRedirect') {
			// Do registration
			var reg = new Registration();
			reg.checkAndDo(() => {
				// Apply stylesheet
				var link = document.createElement('link');
				link.href = Config.StylesheetUrl;
				link.rel = 'stylesheet';
				link.type = 'text/css';
				document.head.appendChild(link);

				// Fetch thread title
				Context.threadTitle = document.title.split('-')[0]; // Output: (3) [C++]Auf privaten statischen Member zugreifen ?
				Context.threadTitle = Context.threadTitle.replace(/\([0-9]+\)/, '').trim();  // Output: [C++]Auf privaten statischen Member zugreifen ?

				// Create arrays
				Client.posts = new Array();
				Client.postIds = new Array();
				
				// Load context
				API.getContext((data: any) => {
					Context.isAdmin = data.isAdmin;
					Context.unreadMentions = data.unreadMentions;

					// Apply tabs and pages
					new VbpcTab();

					if(document.body.id == 'tplThread') {
						this.preparePosts();
						Client.refreshComments();
					}
				});
			});
		}
	}

	private preparePosts() {
		// Apply post options and comments
		// Fetch posts
		var postData = $('ul.wbbThreadPostList').children('li').children('article');

		// Fetch post IDs
		for (var i = 0; i < postData.length; i++) {
			var currentPostId = postData[i].getAttribute('data-object-id');

			if(currentPostId != null) {
				// This post is valid
				var postid = parseInt(currentPostId);
				Client.posts.push(new Post(postid));
				Client.postIds.push(postid);
			}
		}
	}

	public static refreshComments() {
		// Fetch comments and apply them
		API.getComments(Client.postIds, (comments: any) => {
			for(var i = 0; i < Client.postIds.length; i++) {
				var id = Client.postIds[i].toString();
				if(comments.hasOwnProperty(id)) {
					var c = new Array();
					var src = comments[id];

					for(var j = 0; j < src.length; j++) {
						c.push(new VbpcComment(src[j]));
					}

					Client.posts[i].applyComments(c);
				} else {
					Client.posts[i].applyComments(null);
				}
			}
		});
	}
}